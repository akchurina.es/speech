//
//  SectionNumberSetter.swift
//  Speech WatchKit Extension
//
//  Created by Micaela Cavallo on 23/01/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import SwiftUI

struct SectionNumberSetter: View {
    
    @EnvironmentObject var currentSessionSettings : SessionSettings
    
    
    var body: some View {
        
        VStack {
            Text("SECTIONS").fontWeight(.semibold)
            
            HStack {
                //RoundMinusButton().frame(width: 30, height: 30)
                
                Button(action: { self.currentSessionSettings.numbeOfSections -= 1}) {
                    Image(systemName: "minus.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30, alignment: .center)
                } .frame(width: 30, height: 30)
                    .foregroundColor(Color.yellow)
                
                Spacer()
                
                Text("\(currentSessionSettings.numbeOfSections)").font(.system(size: 40, weight: .semibold, design: Font.Design.default))
                Spacer()
                //RoundPlusButton().frame(width: 30, height: 30)
                Button(action: {self.currentSessionSettings.numbeOfSections += 1}) {
                    Image(systemName: "plus.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30, alignment: .center)
                } .frame(width: 30, height: 30)
                    .foregroundColor(Color.yellow)
                
            }.offset(y: -10)
        }
        
    }
}

struct SectionNumberSetter_Previews: PreviewProvider {
    static var previews: some View {
        SectionNumberSetter().environmentObject(SessionSettings())
    }
}
