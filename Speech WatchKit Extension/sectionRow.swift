//
//  sectionRow.swift
//  Speech WatchKit Extension
//
//  Created by Micaela Cavallo on 22/01/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import SwiftUI

struct sectionRow: View {
    
    var body: some View {
        
        
        
        VStack {
            HStack {
                RoundMinusButton().frame(width: 30, height: 30)
                Spacer()
                Text("5").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
                Spacer()
                RoundPlusButton().frame(width: 30, height: 30)
                
            }
            //  Divider()
            
        }
            
        .frame(height: 50, alignment: .center)
        .padding(EdgeInsets(top: 12, leading: 12, bottom: 12, trailing: 12))
        //.background(Color.green)
    }
    
}

struct sectionRow_Previews: PreviewProvider {
    static var previews: some View {
        sectionRow()
    }
}
