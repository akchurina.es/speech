////
////  timer2.swift
////  SwiftUIworkshop WatchKit Extension
////
////  Created by Ekaterina Musiyko on 21/01/2020.
////  Copyright © 2020 Giovanni Monaco. All rights reserved.
////
//
//import SwiftUI
//
//struct TimerView1: View {
//    @State var timeCounter = minutes //minsec.indices
//    @ State private var pausaText = "PAUSA"
//
//    /// Have to mark the variable with @State otherwise Swift complains you are trying to mutate a struct
//    @State private var  countdownTimer: Timer?
//
//    var body: some View {
//
//        VStack (alignment: .center){
//            Spacer()
//            if timeCounter < 10 {
//                Text("\(timeCounter/60):0\(timeCounter%60)")
//                .font(.custom("SFRound", size: 60))
//                .fontWeight(.heavy)
//            }
//            else if timeCounter == 0 {
//                Text("00:00")
//                .font(.custom("SFRound", size: 60))
//                .foregroundColor(.red)
//                .fontWeight(.heavy)
//            }
//            else{
//                Text("\(timeCounter/60):\(timeCounter%60)")
//                .font(.custom("SFRound", size: 60))
//                .fontWeight(.heavy)
//            }
//
//
//
////            Text("\(timeCounter/60):\(timeCounter%60)")
////            .font(.custom("SFRound", size: 60))
////            if timeCounter < 10 { Text("\(timeCounter/60):0\(timeCounter%60)")
////                .font(.custom("SFRound", size: 60))
////                .fontWeight(.heavy)} else if timeCounter < 10 { Text("\(timeCounter/60):0\(timeCounter%60)")} else{
////                Text("00:00")
////                .font(.custom("SFRound", size: 60))
////                    .foregroundColor(.red)
////                .fontWeight(.heavy)
////            }
//            Spacer()
//
//            HStack{
//            Button(action: {
//                if self.pausaText == "PLAY"{
//                self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
//                               /// If the timer has reached 0, then invalidate it
//                               guard self.timeCounter > 0 else {
//                                   self.countdownTimer?.invalidate()
//                                   return
//                               }
//
//                               self.timeCounter -= 1
//                           })
//                    self.pausaText = "PAUSA"
//                } else {
//                print("STOP")
//                self.countdownTimer?.invalidate()
//                    self.pausaText = "PLAY" }
//            }) {
//                Text(pausaText)
//            }
//
//                NavigationLink(destination: main()){
//                Text("STOP")
//                }
//            }
//        }.onAppear {
//            /// Create the countdown timer
//            self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
//                /// If the timer has reached 0, then invalidate it
//                guard self.timeCounter > 0 else {
//                    self.countdownTimer?.invalidate()
//                    return
//                }
//
//                /// Otherwise decrement the counter
//                self.timeCounter -= 1
//            })
//        }
//    .navigationBarTitle("SPEECH")
//
//    }
//}
//struct TimerView_Previews: PreviewProvider {
//    static var previews: some View {
//        TimerView()
//    }
//}
