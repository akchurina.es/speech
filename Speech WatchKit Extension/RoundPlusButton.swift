//
//  RoundPlusButton.swift
//  newTimer WatchKit Extension
//
//  Created by Micaela Cavallo on 21/01/2020.
//  Copyright © 2020 Micaela Cavallo. All rights reserved.
//

import SwiftUI

struct RoundPlusButton: View {
    var body: some View {
        GeometryReader { geometry in

             Button(action: {
//                minutes += 1
             }) {
                 VStack {
                     Image(systemName: "plus.circle.fill")
                        .font(.system(size: 30))
                 }
                 .frame(width: geometry.size.width, height: geometry.size.height)
             }
             .frame(width: geometry.size.width, height: geometry.size.height)
                 .clipShape(Circle())
             .foregroundColor(Color(#colorLiteral(red: 1, green: 0.8540648818, blue: 0.375326544, alpha: 1)))
         }
    }
}

struct RoundPlusButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundPlusButton()
    }
}
