//
//  TotalTimeSetter.swift
//  Speech WatchKit Extension
//
//  Created by Micaela Cavallo on 22/01/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import SwiftUI

struct TotalTimeSetter: View {
    
    @EnvironmentObject var currentSessionSettings : SessionSettings
    
    var body: some View {
        VStack {
            Text("TOTAL TIME").fontWeight(.semibold)
            
            HStack {
                // RoundMinusButton().frame(width: 30, height: 30)
                Button(action: { self.currentSessionSettings.totalDuration -= 1}) {
                    Image(systemName: "minus.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30, alignment: .center)
                }.frame(width: 30, height: 30)
                    .foregroundColor(Color.yellow)
                Spacer()
                HStack {
                    Text("\(currentSessionSettings.totalDuration)").font(.system(size: 40, weight: .semibold, design: Font.Design.default))
                    Text("min").font(.system(size: 24, weight: .regular, design: Font.Design.default))
                }
                
                
                Spacer()
                // RoundPlusButton().frame(width: 30, height: 30)
                
                Button(action: { self.currentSessionSettings.totalDuration += 1}) {
                    Image(systemName: "plus.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30, alignment: .center)
                }
                    
                .frame(width: 30, height: 30)
                .foregroundColor(Color.yellow)
                
            }.offset(y: -10)
        }
        
    }
}

struct TotalTimeSetter_Previews: PreviewProvider {
    static var previews: some View {
        TotalTimeSetter().environmentObject(SessionSettings())
    }
}
