//
//  timer2.swift
//  SwiftUIworkshop WatchKit Extension
//
//  Created by Ekaterina Musiyko on 21/01/2020.
//  Copyright © 2020 Giovanni Monaco. All rights reserved.
//


import SwiftUI
import WatchKit
import UIKit


struct TimerView: View  {
    
   
    
    @Environment(\.presentationMode) var presentation

   @State var timeCounter: Int = minutes * 60
    @ State private var pausaText = "PAUSA"
    /// Have to mark the variable with @State otherwise Swift complains you are trying to mutate a struct
    @State private var countdownTimer: Timer?
    
    @EnvironmentObject var currentSessionSettings : SessionSettings
    
    
    var body: some View {
        
        VStack {
            if timeCounter%60 < 10 && timeCounter > 0 {
                Text("\(timeCounter/60):0\(timeCounter%60)")
                .font(.custom("SFRound", size: 60))
                .fontWeight(.heavy)
            } else if timeCounter > 0 {
                Text("\(timeCounter/60):\(timeCounter%60)")
                .font(.custom("SFRound", size: 60))
                .fontWeight(.heavy)
            } else {
                Text("00:00")
                .fontWeight(.heavy)
                .font(.custom("SFRound", size: 60))
            }
            
            Spacer()
            
            HStack{
            Button(action: {
                if self.pausaText == "PLAY"{
                self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
                               /// If the timer has reached 0, then invalidate it
                               guard self.timeCounter > 0 else {
                                WKInterfaceDevice.current().play(.success)
                                self.countdownTimer?.invalidate()
                                   return
                               }
                               
                               self.timeCounter -= 1
                           })
                    self.pausaText = "PAUSA"
                } else {
                self.countdownTimer?.invalidate()
                    self.pausaText = "PLAY" }
            })
            {Text(pausaText)}

                Button(action: {
                print("STOP")
                self.countdownTimer?.invalidate()
                self.timeCounter = 0
                self.presentation.wrappedValue.dismiss()
            })
                {Text("STOP")}
            }
        }.onAppear {
            /// Create the countdown timer
            self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
                /// If the timer has reached 0, then invalidate it
                if self.timeCounter == 30 {
                    WKInterfaceDevice.current().play(.retry)
                }
                
                guard self.timeCounter > 0 else {
                    self.countdownTimer?.invalidate()
                   WKInterfaceDevice.current().play(.stop)
                    return
                }
                
                /// Otherwise decrement the counter
                self.timeCounter -= 1
            })
        }
    .navigationBarTitle("SPEECH")
    
    }
}


struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView()
    }
}
