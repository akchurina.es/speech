
//
//  SessionSettings.swift
//  Speech WatchKit Extension
//
//  Created by Micaela Cavallo on 23/01/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

class Section : ObservableObject {
    
    @Published var minutes : Int = 5
}

class SessionSettings: ObservableObject {
    @Published var totalDuration: Int = 15
    @Published var numbeOfSections: Int = 3
    @Published var arrayOfSections : [Section] = [Section(), Section(), Section()]
}
