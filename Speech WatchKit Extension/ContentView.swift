//
//  ContentView.swift
//  newTimer WatchKit Extension
//
//  Created by Micaela Cavallo on 21/01/2020.
//  Copyright © 2020 Micaela Cavallo. All rights reserved.
//

import SwiftUI

struct ContentViewBis: View {
    
    var sessionSettings = SessionSettings()
    var minutes = 1
    var body: some View {
        
        ScrollView {
            VStack {
                
                TotalTimeSetter().environmentObject(sessionSettings)
            }
            //Spacer()
            SectionNumberSetter().environmentObject(sessionSettings)
            
            Spacer()
            
            
            VStack {
                Text("TIME PER SECTION").fontWeight(.semibold)
                
                ZStack {
                    RoundedRectangle(cornerRadius: 40)                .stroke(Color.yellow, lineWidth: 2)
                    VStack(spacing: 0) {
                        sectionRow()
                        Divider()
                        sectionRow()
                        Divider()
                        sectionRow()
                    }
                }
                
                
                VStack {
                    HStack {
                        Button(action: {}) {
                            Text("Save")
                        }
                        NavigationLink("Start", destination: TimerView()).background(Color.gray)
                            .cornerRadius(22)
                    }
                }
                
            }
            
        }
//        ScrollView {
//            VStack {
//                Text("DURATION")
//
//                HStack {
//                    RoundMinusButton().frame(width: 30, height: 30)
//                    Spacer()
//                    HStack {
//                        Text("15").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
//                        Text("min").font(.system(size: 24, weight: .semibold, design: Font.Design.rounded))
//                    }
//
//                    Spacer()
//                    RoundPlusButton().frame(width: 30, height: 30)
//
//                }
//            }
//            Spacer()
//
//            VStack {
//                Text("NUMBER OF SECTIONS")
//
//                HStack {
//                    RoundMinusButton().frame(width: 30, height: 30)
//                    Spacer()
//                    Text("3").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
//                    Spacer()
//                    RoundPlusButton().frame(width: 30, height: 30)
//
//                }
//            }
//            Spacer()
//
//            VStack {
//                Text("DURATION OF SECTIONS")
//
//                VStack {
//                    HStack {
//                        RoundMinusButton().frame(width: 30, height: 30)
//                        Spacer()
//                        Text("5").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
//                        Spacer()
//                        RoundPlusButton().frame(width: 30, height: 30)
//
//                    }
//
//                    Divider()
//
//                    HStack {
//                        RoundMinusButton().frame(width: 30, height: 30)
//                        Spacer()
//                        Text("5").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
//                        Spacer()
//                        RoundPlusButton().frame(width: 30, height: 30)
//
//                    }
//                    Divider()
//
//                    HStack {
//                        RoundMinusButton().frame(width: 30, height: 30)
//                        Spacer()
//                        Text("5").font(.system(size: 40, weight: .semibold, design: Font.Design.rounded))
//                        Spacer()
//                        RoundPlusButton().frame(width: 30, height: 30)
//
//                    }
//                    Divider()
//                }
//                .padding(EdgeInsets(top: 0, leading: 12, bottom: -25, trailing: 12))
//                .border(Color(#colorLiteral(red: 0.4430960417, green: 0.443163693, blue: 0.4430812597, alpha: 1)), width: 3)
//                .cornerRadius(20)
//
//
//                VStack {
//                    HStack {
//                       Button(action: {}) {
//                           Text("Save")
//                       }
//                        NavigationLink("Start", destination: TimerView())
//
//                    }
//                }
//
//            }
//
//        }
    }
}

struct ContentViewBis_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewBis()
    }
}


//
