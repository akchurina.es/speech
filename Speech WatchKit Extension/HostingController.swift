//
//  HostingController.swift
//  Speech WatchKit Extension
//
//  Created by Ekaterina Musiyko on 17/01/2020.
//  Copyright © 2020 Ekaterina Akchurina. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<main> {
    override var body: main {
        return main()
    }
}
