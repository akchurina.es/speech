//
//  main.swift
//  heartBeat WatchKit Extension
//
//  Created by Ahmed Chouat on 22/01/2020.
//  Copyright © 2020 Ahmed Chouat. All rights reserved.
//

import SwiftUI

struct main: View {
    var body: some View {
        NavigationLink(destination: ContentViewBis()){
            Image(systemName: "plus")
                .font(.system(size: 60))
                .foregroundColor(.black)
        }
        .buttonStyle(PlainButtonStyle())
        .padding(30)
        .background(Color(#colorLiteral(red: 0.9983314872, green: 0.857950449, blue: 0.5181956291, alpha: 1)))
            
            //                        .cornerRadius(40)
            .clipShape(Circle())
    }
    
    struct main_Previews: PreviewProvider {
        static var previews: some View {
            main()
        }
    }
    
}
